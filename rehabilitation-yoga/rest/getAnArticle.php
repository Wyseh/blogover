<?php
/**
 * Created by PhpStorm.
 * User: baptiste
 * Date: 10/5/18
 * Time: 11:32 AM
 */

# Load the json file
$url = 'articles.json'; // path to your JSON file
$data = file_get_contents($url); // put the contents of the file into a variable
$articles = json_decode($data, TRUE); // decode the JSON feed

# Setting up variables for the json and the get
$getKey = "article";
$jsonKey = "articles";

# Check if there is a getAnArticle.php?article=x
if (isset($_GET[$getKey])){

    # Get the article requested
    $getArticle = $_GET[$getKey];

    # Check if the article exists
    if($getArticle <= count($articles[$jsonKey]) ){
        # Display article details
        echo 'Article ' . $getArticle . ' correspond to :' . '</br>';
        foreach ($articles[$jsonKey][$getArticle] as $key => $data) {
            echo $key . ' : ' . $data . "</br>";
        }
    } else{
        # If the article does not exists
        print("Article does not exist");
    }
}else{
    # If there is no getAnArticle.php?article=x
    echo "Error, please check the url or go back to the home page";
}

?>