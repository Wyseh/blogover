<!DOCTYPE html>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="user-scalable=0, width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>About - Belle &amp; Carrie Rehabilitation Yoga Web Template</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/mobile.css">
    <script type='text/javascript' src='js/mobile.js'></script>
    <script type="text/javascript" src="js/ajax.js"></script>
    <script type="text/javascript" src="identification/js/main.js"></script>
</head>
<body>
<div id="header">
    <h1><a href="index.php">Belle &amp; Carrie <span>Rehabilitation Yoga</span></a></h1>
    <ul id="navigation">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li class="current">
            <a href="about.php">About</a>
        </li>
        <li>
            <a href="classes.php">Classes</a>
            <ul>
                <li>
                    <a href="instructors.php">Instructors</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="contact.php">Contact</a>
        </li>
        <li>
            <a href="blog.php">Blog</a>
        </li>
        <li>
            <a href="identification/index.php">Inscription</a>
        </li>
    </ul>
</div>