<?php

include "header.php";
?>
<body>
	<div id="header">
		<h1><a href="index.php">Belle &amp; Carrie <span>Rehabilitation Yoga</span></a></h1>
		<ul id="navigation">
			<li>
				<a href="index.php">Home</a>
			</li>
			<li>
				<a href="about.php">About</a>
			</li>
			<li>
				<a href="classes.php">Classes</a>
				<ul>
					<li>
						<a href="instructors.php">Instructors</a>
					</li>
				</ul>
			</li>
			<li class="current">
				<a href="contact.php">Contact</a>
			</li>
			<li>
				<a href="blog.php">Blog</a>
			</li>
		</ul>
	</div>
	<div id="body">
		<h2>Contact</h2>
		<form action="index.php">
			<h3>Inquiries</h3>
			<label for="name">
				<span>Name</span>
				<input type="text" id="name">
			</label>
			<label for="email">
				<span>Email</span>
				<input type="text" id="email">
			</label>
			<label for="subject">
				<span>Subject</span>
				<input type="text" id="subject">
			</label>
			<label for="message">
				<span>Message</span>
				<textarea name="message" id="message" cols="30" rows="10"></textarea>
			</label>
			<input type="submit" id="send" value="Send">
		</form>
	</div>
	<div id="footer">
		<div>
			<span>123 St. City Location, Country | 987654321</span>
			<p>
				&copy; 2023 by Belle &amp; Carrie Rehabilitation Yoga. All rights reserved.
			</p>
		</div>
		<div id="connect">
			<a href="https://freewebsitetemplates.com/go/facebook/" id="facebook" target="_blank">Facebook</a>
			<a href="https://freewebsitetemplates.com/go/twitter/" id="twitter" target="_blank">Twitter</a>
			<a href="https://freewebsitetemplates.com/go/googleplus/" id="googleplus" target="_blank">Google&#43;</a>
			<a href="https://freewebsitetemplates.com/go/pinterest/" id="pinterest" target="_blank">Pinterest</a>
		</div>
	</div>
<?php

include "footer.php"

?>